package controllers

import (
	"net/http"

	"bitbucket.org/boolow5/bolinventory/api/logger"
	"bitbucket.org/boolow5/bolinventory/api/models"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

// AddStore adds new store or shop to the database
func AddStore(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	admin := models.User{}
	err := models.GetItemByID(userID, &admin)
	if err != nil {
		logger.Error("finding admin by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}
	if admin.Level < models.FifthUserLevel {
		logger.Errorf("user level very low: %d", admin.Level)
		AbortWithError(c, http.StatusBadRequest, NewError(PermissionError, err.Error()))
		return
	}
	input := models.Store{}
	err = c.Bind(&input)
	if err != nil {
		logger.Error("bind store failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}

	errs := models.Save(&input)
	if errs != nil {
		logger.Error("saving error: ", err)
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(SavingItemError, errs))
		return
	}
	c.JSON(http.StatusOK, gin.H{"store": input})
}

// UpdateStore updates store or shop data in the database
func UpdateStore(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	admin := models.User{}
	err := models.GetItemByID(userID, &admin)
	if err != nil {
		logger.Error("finding admin by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}
	if admin.Level < models.FifthUserLevel {
		logger.Errorf("user level very low: %d", admin.Level)
		AbortWithError(c, http.StatusBadRequest, NewError(PermissionError, err.Error()))
		return
	}
	input := models.Store{}
	err = c.Bind(&input)
	if err != nil {
		logger.Error("bind store failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}

	var store models.Store
	err = models.GetItemByID(input.ID, &store)
	if err != nil {
		logger.Error("finding store by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(NotFoundError, err.Error()))
		return
	}

	store.Name = input.Name
	store.ContactInfo = input.ContactInfo
	store.Location = input.Location

	errs := models.Update(&store)
	if errs != nil {
		logger.Error("saving error: ", err)
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(SavingItemError, errs))
		return
	}
	c.JSON(http.StatusOK, gin.H{"store": input})
}

// DeleteStore removes store or shop data from the database
func DeleteStore(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	admin := models.User{}
	err := models.GetItemByID(userID, &admin)
	if err != nil {
		logger.Error("finding admin by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}
	if admin.Level < models.FifthUserLevel {
		logger.Errorf("user level very low: %d", admin.Level)
		AbortWithError(c, http.StatusBadRequest, NewError(PermissionError, err.Error()))
		return
	}
	input := models.Store{}
	err = c.Bind(&input)
	if err != nil {
		logger.Error("bind store failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}

	var store models.Store
	err = models.GetItemByID(input.ID, &store)
	if err != nil {
		logger.Error("finding store by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(NotFoundError, err.Error()))
		return
	}

	err = models.Remove(&store)
	if err != nil {
		logger.Error("finding store by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(UknownError, err.Error()))
		return
	}
	c.JSON(http.StatusOK, gin.H{"store": input})
}
