package models

import (
	"fmt"
	"time"

	"gopkg.in/mgo.v2/bson"
)

// Store represents an inventory store or a shop
type Store struct {
	ID          bson.ObjectId `json:"id" bson:"_id"`
	Name        string        `json:"name" bson:"name"`
	Location    Location      `json:"location" bson:"location"`
	ContactInfo ContactInfo   `json:"contact_info" bson:"contact_info"`

	Timestamp `bson:",inline"`
}

// GetColName satisfies DBObject interface
func (s Store) GetColName() string {
	return StoreCollection
}

// GetID satisfies DBObject interface
func (s Store) GetID() bson.ObjectId {
	return s.ID
}

// SetID satisfies DBObject interface
func (s *Store) SetID(id bson.ObjectId) {
	s.ID = id
}

// SetTimestamp satisfies DBObject interface
func (s *Store) SetTimestamp(isCreate bool) {
	now := time.Now()
	if isCreate {
		s.CreatedAt = now
		s.UpdatedAt = now
	} else {
		s.UpdatedAt = now
	}
}

// IsValid satisfies DBObject interface
func (s Store) IsValid() (valid bool, errs []error) {
	valid = bson.IsObjectIdHex(s.ID.Hex())
	if !valid {
		return valid, []error{fmt.Errorf("Invalid store id")}
	}
	return valid, nil
}

// AutoSetID satisfies DBObject interface, to control setting item ID behavior
func (s Store) AutoSetID() bool {
	return true
}
