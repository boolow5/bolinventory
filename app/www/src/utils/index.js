var emailIsValid = function (email) {
  let re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
  return re.test(email)
}

var phoneIsValid = function (phone) {
  let num = cleanPhoneNumber(phone)
  let number = parseInt(num)
  let len = number.toString().length
  return len === 9 || len === 10 || len === 12
}

var cleanPhoneNumber = function (phone) {
  if (typeof phone !== 'string') {
    return ''
  }
  return phone.replace('+', '').split(' ').join('').split('-').join('')
}

var ParseQRCodeURL = function (url) {
  if (!url || typeof url !== 'string') {
    return {}
  }
  let parts = url.split('?')
  if (parts.length < 1) {
    return {}
  }
  if (parts.length > 1) {
    let domain = parts[0].replace('/qrcode', '')
    let queryParts = parts[1]
    let ps = queryParts.split('&')
    let queries = {domain}
    for (let i = 0; i < ps.length; i++) {
      let kv = ps[i].split('=')
      if (kv.length === 2) {
        queries[kv[0]] = kv[1]
      } else if (kv.length === 1) {
        queries[kv[0]] = true
      }
    }
    return queries
  }
  return {}
}

var isIDHex = function (id) {
  return typeof id === 'string' && id.length === 24
}

export default {
  emailIsValid,
  phoneIsValid,
  cleanPhoneNumber,
  ParseQRCodeURL,
  isIDHex
}
