package models

import "gopkg.in/mgo.v2/bson"

// Address represents a location name data
type Address struct {
	Street   string
	House    string
	Floor    string
	District string
	City     string
	Country  string
}

// Location represents a point on earth
type Location struct {
	ID      bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Long    float64       `json:"long" bson:"long"`
	Lat     float64       `json:"lat" bson:"lat"`
	Backend string        `json:"backed" bson:"backed"`
	Address Address       `json:"address" bson:"address"`

	Timestamp `bson:",inline"`
}

// ContactInfo represents contact info for a person, organization and etc.
type ContactInfo struct {
	PhoneNumbers []string `json:"phone_numbers" bson:"phone_numbers"`
	Emails       []string `json:"emails" bson:"emails"`
}

// Image represents a picture
type Image struct {
	URL         string `json:"url" bson:"url"`
	Title       string `json:"title" bson:"title"`
	Description string `json:"description" bson:"description"`
}
