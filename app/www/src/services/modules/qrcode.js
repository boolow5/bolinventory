import q from 'q'

var Scanner = {}

var initQRCode = function () {
  console.log('QR.initQRCode')
  var deferred = q.defer()
  if (window.QRScanner && typeof window.QRScanner.prepare === 'function') {
    window.QRScanner.prepare(function (err, status) {
      if (err) {
        deferred.reject(err)
      } else {
        Scanner = window.QRScanner
        deferred.resolve(status)
      }
    })
  } else {
    deferred.reject(new Error('plugin QRScanner is not ready'))
  }
  return deferred.promise
}

var scan = function () {
  console.log('QR.scan')
  var deferred = q.defer()
  if (typeof Object(Scanner).scan !== 'function') {
    deferred.reject(new Error('plugin QRScanner is not ready'))
  } else {
    Scanner.scan(function (err, text) {
      if (err) {
        deferred.reject(err)
      } else {
        deferred.resolve({data: text})
      }
    })
  }
  return deferred.promise
}

var cancelScan = function () {
  console.log('QR.cancelScan')
  var deferred = q.defer()
  if (typeof Object(Scanner).cancelScan !== 'function') {
    deferred.reject(new Error('plugin QRScanner is not ready'))
  } else {
    Scanner.cancelScan(function (status) {
      deferred.resolve(status)
    })
  }
  return deferred.promise
}

var show = function () {
  console.log('QR.show')
  var deferred = q.defer()
  if (typeof Object(Scanner).show !== 'function') {
    deferred.reject(new Error('plugin QRScanner is not ready'))
  } else {
    Scanner.show(function (status) {
      deferred.resolve(status)
    })
  }
  return deferred.promise
}

var hide = function () {
  console.log('QR.hide')
  var deferred = q.defer()
  if (typeof Object(Scanner).hide !== 'function') {
    deferred.reject(new Error('plugin QRScanner is not ready'))
  } else {
    Scanner.hide(function (status) {
      deferred.resolve(status)
    })
  }
  return deferred.promise
}

var enableLight = function () {
  console.log('QR.enableLight')
  var deferred = q.defer()
  if (typeof Object(Scanner).enableLight !== 'function') {
    deferred.reject(new Error('plugin QRScanner is not ready'))
  } else {
    Scanner.enableLight(function (status) {
      deferred.resolve(status)
    })
  }
  return deferred.promise
}

var disableLight = function () {
  console.log('QR.disalbeLight')
  var deferred = q.defer()
  if (typeof Object(Scanner).disableLight !== 'function') {
    deferred.reject(new Error('plugin QRScanner is not ready'))
  } else {
    Scanner.disableLight(function (status) {
      deferred.resolve(status)
    })
  }
  return deferred.promise
}

var useFrontCamera = function () {
  console.log('QR.useFrontCamera')
  var deferred = q.defer()
  if (typeof Object(Scanner).useFrontCamera !== 'function') {
    deferred.reject(new Error('plugin QRScanner is not ready'))
  } else {
    Scanner.useFrontCamera(function (status) {
      deferred.resolve(status)
    })
  }
  return deferred.promise
}

var useBackCamera = function () {
  console.log('QR.useBackCamera')
  var deferred = q.defer()
  if (typeof Object(Scanner).useBackCamera !== 'function') {
    deferred.reject(new Error('plugin QRScanner is not ready'))
  } else {
    Scanner.useBackCamera(function (status) {
      deferred.resolve(status)
    })
  }
  return deferred.promise
}

var pausePreview = function () {
  console.log('QR.pausePreview')
  var deferred = q.defer()
  if (typeof Object(Scanner).pausePreview !== 'function') {
    deferred.reject(new Error('plugin QRScanner is not ready'))
  } else {
    Scanner.pausePreview(function (status) {
      deferred.resolve(status)
    })
  }
  return deferred.promise
}

var resumePreview = function () {
  console.log('QR.resumePreview')
  var deferred = q.defer()
  if (typeof Object(Scanner).resumePreview !== 'function') {
    deferred.reject(new Error('plugin QRScanner is not ready'))
  } else {
    Scanner.resumePreview(function (status) {
      deferred.resolve(status)
    })
  }
  return deferred.promise
}

var getStatus = function () {
  console.log('QR.getStatus')
  var deferred = q.defer()
  if (typeof Object(Scanner).getStatus !== 'function') {
    deferred.reject(new Error('plugin QRScanner is not ready'))
  } else {
    Scanner.getStatus(function (status) {
      deferred.resolve(status)
    })
  }
  return deferred.promise
}

var openSettings = function () {
  console.log('QR.openSettings')
  var deferred = q.defer()
  if (typeof Object(Scanner).openSettings !== 'function') {
    deferred.reject(new Error('plugin QRScanner is not ready'))
  } else {
    if (!status.canOpenSettings) {
      deferred.reject(new Error('we do not have your permission to open settings'))
    } else {
      Scanner.openSettings()
      deferred.resolve('OK')
    }
  }
  return deferred.promise
}

var clearAll = function () {
  console.log('QR.clearAll')
  var deferred = q.defer()
  if (typeof Object(Scanner).destroy !== 'function') {
    deferred.reject(new Error('plugin QRScanner is not ready'))
  } else {
    Scanner.destroy(function (status) {
      deferred.resolve(status)
    })
  }
  return deferred.promise
}

export default {
  initQRCode,
  scan,
  cancelScan,
  show,
  hide,
  enableLight,
  disableLight,
  useFrontCamera,
  useBackCamera,
  pausePreview,
  resumePreview,
  getStatus,
  openSettings,
  clearAll
}
