package models

const (
	// UserCollection represents user database collection
	UserCollection = "user"
	// StoreCollection represents store database collection
	StoreCollection = "store"
	// ProductCollection represents product database collection
	ProductCollection = "product"
	// CartCollection represents cart database collection
	CartCollection = "car"
)
