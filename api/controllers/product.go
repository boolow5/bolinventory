package controllers

import (
	"net/http"

	"bitbucket.org/boolow5/bolinventory/api/logger"
	"bitbucket.org/boolow5/bolinventory/api/models"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

// AddProduct adds new product or shop to the database
func AddProduct(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	emp := models.User{}
	err := models.GetItemByID(userID, &emp)
	if err != nil {
		logger.Error("finding emp by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}
	if emp.Level < models.SecondUserLevel {
		logger.Errorf("user level very low: %d", emp.Level)
		AbortWithError(c, http.StatusBadRequest, NewError(PermissionError, err.Error()))
		return
	}
	input := models.Product{}
	err = c.Bind(&input)
	if err != nil {
		logger.Error("bind product failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}

	errs := models.Save(&input)
	if errs != nil {
		logger.Error("saving error: ", err)
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(SavingItemError, errs))
		return
	}
	c.JSON(http.StatusOK, gin.H{"product": input})
}

// UpdateProduct updates product or shop data in the database
func UpdateProduct(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	emp := models.User{}
	err := models.GetItemByID(userID, &emp)
	if err != nil {
		logger.Error("finding emp by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}
	if emp.Level < models.ThirdUserLevel {
		logger.Errorf("user level very low: %d", emp.Level)
		AbortWithError(c, http.StatusBadRequest, NewError(PermissionError, err.Error()))
		return
	}
	input := models.Product{}
	err = c.Bind(&input)
	if err != nil {
		logger.Error("bind product failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}

	var product models.Product
	err = models.GetItemByID(input.ID, &product)
	if err != nil {
		logger.Error("finding product by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(NotFoundError, err.Error()))
		return
	}

	product.Name = input.Name
	product.BarcodeID = input.BarcodeID
	product.StoreInventories = input.StoreInventories

	errs := models.Update(&product)
	if errs != nil {
		logger.Error("saving error: ", err)
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(SavingItemError, errs))
		return
	}
	c.JSON(http.StatusOK, gin.H{"product": input})
}

// DeleteProduct removes product or shop data from the database
func DeleteProduct(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	emp := models.User{}
	err := models.GetItemByID(userID, &emp)
	if err != nil {
		logger.Error("finding emp by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}
	if emp.Level < models.FourthUserLevel {
		logger.Errorf("user level very low: %d", emp.Level)
		AbortWithError(c, http.StatusBadRequest, NewError(PermissionError, err.Error()))
		return
	}
	input := models.Product{}
	err = c.Bind(&input)
	if err != nil {
		logger.Error("bind product failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}

	var product models.Product
	err = models.GetItemByID(input.ID, &product)
	if err != nil {
		logger.Error("finding product by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(NotFoundError, err.Error()))
		return
	}

	err = models.Remove(&product)
	if err != nil {
		logger.Error("finding product by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(UknownError, err.Error()))
		return
	}
	c.JSON(http.StatusOK, gin.H{"product": input})
}
