package db

import (
	"fmt"
	"math/rand"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var (
	maxLimit = 1000
	pageSize = 20
)

func Create(c string, data interface{}) error {
	session := GetMongoSession()
	defer session.Close()
	return session.DB(dbName).C(c).Insert(data)
}

func CreateStrong(c string, data interface{}) error {
	session := GetMongoSession()
	defer session.Close()
	session.SetMode(mgo.Strong, true)
	return session.DB(dbName).C(c).Insert(data)
}

// CreateBulk inserts items in bulk
func CreateBulk(c string, data ...interface{}) {
	session := GetMongoSession()
	defer session.Close()
	session.DB(dbName).C(c).Bulk().Insert(data...)
}

func Update(c string, id interface{}, data interface{}) error {
	session := GetMongoSession()
	defer session.Close()
	return session.DB(dbName).C(c).Update(bson.M{"_id": id}, data)
}

func SelectStrong(c string, find *bson.M, selector interface{}, data interface{}) error {
	session := GetMongoSession()
	session.SetMode(mgo.Strong, true)
	defer session.Close()
	return session.DB(dbName).C(c).Find(find).Select(selector).All(data)
}

func GetDistincStrong(c string, find *bson.M, distinctKey string, data interface{}) error {
	session := GetMongoSession()
	session.SetMode(mgo.Strong, true)
	defer session.Close()
	return session.DB(dbName).C(c).Find(find).All(data)
}

func UpdateStrong(c string, id interface{}, data interface{}) error {
	session := GetMongoSession()
	session.SetMode(mgo.Strong, true)
	defer session.Close()
	return session.DB(dbName).C(c).Update(bson.M{"_id": id}, data)
}

func UpdateStrongDeals(c string, id int64, data interface{}) error {
	session := GetMongoSession()
	session.SetMode(mgo.Strong, true)
	defer session.Close()
	err := session.DB(dbName).C(c).Update(bson.M{"deal_id": id}, data)
	if err != nil {
		return err

	}
	return nil
}

func Retrieve(c string, data interface{}) error {
	session := GetMongoSession()
	defer session.Close()
	return session.DB(dbName).C(c).Find(nil).All(data)
}

func RetrieveStrong(c string, data interface{}) error {
	session := GetMongoSession()
	session.SetMode(mgo.Strong, true)
	defer session.Close()
	return session.DB(dbName).C(c).Find(nil).All(data)
}

func FindOne(c string, find *bson.M, data interface{}) error {
	session := GetMongoSession()
	defer session.Close()
	return session.DB(dbName).C(c).Find(find).One(data)
}

func FindOneByColumns(c string, find *bson.M, data interface{}, columns ...string) error {
	session := GetMongoSession()
	defer session.Close()
	selector := bson.M{}
	for _, column := range columns {
		selector[column] = 1
	}
	return session.DB(dbName).C(c).Find(find).Select(selector).One(data)
}

func FindOneStrong(c string, find *bson.M, data interface{}) error {
	session := GetMongoSession()
	session.SetMode(mgo.Strong, true)
	defer session.Close()
	return session.DB(dbName).C(c).Find(find).One(data)
}

func FindAll(c string, find *bson.M, data interface{}) error {
	db := Get()
	if db == nil {
		return fmt.Errorf("connection error")
	}
	defer db.Session.Close()
	return db.C(c).Find(find).Limit(maxLimit).All(data)
}

func FindAllSort(c string, find *bson.M, data interface{}, sort ...string) error {
	db := Get()
	defer db.Session.Close()
	if len(sort) == 0 {
		sort = append(sort, []string{"-created_at"}...)
	}
	return db.C(c).Find(find).Sort(sort...).Limit(maxLimit).All(data)
}

func FindPage(c string, find *bson.M, page int, data interface{}) error {
	db := Get()
	defer db.Session.Close()
	return db.C(c).Find(find).Skip(page * pageSize).Limit(pageSize).All(data)
}

func FindAtPostion(c string, find *bson.M, position int, data interface{}) error {
	db := Get()
	defer db.Session.Close()
	return db.C(c).Find(find).Skip(position).One(data)
}

func FindOneRandom(c string, find *bson.M, data interface{}) error {
	count, err := Count(c, find)
	if err != nil {
		return err
	}

	if count == 0 {
		return fmt.Errorf("Data not sufficient")
	}

	db := Get()
	defer db.Session.Close()
	randNo := rand.Intn(count)
	return db.C(c).Find(find).Skip(randNo).One(data)
}

func FindPageWithLimit(c string, find *bson.M, page int, limit int, data interface{}) error {
	db := Get()
	defer db.Session.Close()
	return db.C(c).Find(find).Skip(page * limit).Limit(limit).All(data)
}

func FindWithLimit(c string, find *bson.M, limit int, data interface{}) error {
	db := Get()
	defer db.Session.Close()
	return db.C(c).Find(find).Limit(limit).All(data)
}

func FindPageSort(c string, find *bson.M, page int, data interface{}, sort ...string) error {
	session := GetMongoSession()
	session.SetMode(mgo.Strong, true)
	defer session.Close()
	return session.DB(dbName).C(c).Find(find).Sort(sort...).Skip(page * pageSize).Limit(pageSize).All(data)
}

func FindSortLimit(c string, find *bson.M, limit int, data interface{}, sort ...string) error {
	session := GetMongoSession()
	session.SetMode(mgo.Strong, true)
	defer session.Close()
	if len(sort) == 0 {
		sort = append(sort, []string{"-created_at"}...)
	}
	return session.DB(dbName).C(c).Find(find).Sort(sort...).Limit(limit).All(data)
}

func Pipe(c string, pipe *[]bson.M, limit int, data interface{}) error {
	session := GetMongoSession()
	session.SetMode(mgo.Strong, true)
	defer session.Close()
	return session.DB(dbName).C(c).Pipe(pipe).All(data)
}

func FindPageStrong(c string, find *bson.M, page int, data interface{}) error {
	session := GetMongoSession()
	session.SetMode(mgo.Strong, true)
	defer session.Close()
	return session.DB(dbName).C(c).Find(find).Skip(page * pageSize).Limit(pageSize).All(data)
}

func FindAndReplace(c string, find *bson.M, update *bson.M, data interface{}) (*mgo.ChangeInfo, error) {
	session := GetMongoSession()
	session.SetMode(mgo.Strong, true)
	defer session.Close()

	change := mgo.Change{
		Update:    update,
		ReturnNew: true,
	}

	return session.DB(dbName).C(c).Find(find).Apply(change, data)
}

func Count(c string, find *bson.M) (int, error) {
	session := GetMongoSession()
	defer session.Close()
	return session.DB(dbName).C(c).Find(find).Count()
}

func DeleteId(c string, ID interface{}) error {
	session := GetMongoSession()
	defer session.Close()
	return session.DB(dbName).C(c).RemoveId(ID)
}

func DeleteMany(c string, find *bson.M) (*mgo.ChangeInfo, error) {
	session := GetMongoSession()
	defer session.Close()
	return session.DB(dbName).C(c).RemoveAll(find)
}

func AddUniqueIndex(collection string, keys ...string) error {
	session := GetMongoSession()
	defer session.Close()
	index := mgo.Index{
		Key:        keys,
		Unique:     true,
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}
	return session.DB(dbName).C(collection).EnsureIndex(index)
}

func Add2DIndex(collection string, keys ...string) error {
	session := GetMongoSession()
	index := mgo.Index{
		Key:        keys,
		Unique:     false,
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}
	return session.DB(dbName).C(collection).EnsureIndex(index)
}

func DropCollection(c string) error {
	session := GetMongoSession()
	return session.DB(dbName).C(c).DropCollection()
}

func AddIndex(collection string, keys ...string) error {
	session := GetMongoSession()
	index := mgo.Index{
		Key:      keys,
		Unique:   false,
		DropDups: false,
	}
	return session.DB(dbName).C(collection).EnsureIndex(index)
}
