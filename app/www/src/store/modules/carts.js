const state = {
  lastCartID: 0,
  carts: [
    {
      id: 1,
      items: [],
      customer: {
        id: 1,
        name: 'Uknown',
        phone: ''
      },
      created_at: new Date(),
      created_by: {
        id: 1,
        name: 'Unknown'
      },
      is_active: true
    },
    {
      id: 2,
      items: [],
      customer: {
        id: 2,
        name: 'Uknown',
        phone: ''
      },
      created_at: new Date(),
      created_by: {
        id: 1,
        name: 'Unknown'
      },
      is_active: false
    }
  ]
}
const getters = {
  getActiveCart (state) {
    for (let i = 0; i < state.carts.length; i++) {
      if (state.carts[i].is_active) {
        return state.carts[i]
      }
    }
  },
  getCarts (state) {
    return state.carts
  }
}
const mutations = {
  setActiveCart (state, payload) {
    for (let i = 0; i < state.carts.length; i++) {
      if (state.carts[i].id === payload) {
        state.carts[i].is_active = true
      } else {
        state.carts[i].is_active = false
      }
    }
  },
  incrementLastCartID (state, payload) {
    state.lastCartID++
  },
  addNewCart (state, payload) {
    state.carts.push({
      id: state.lastCartID,
      items: [],
      customer: {
        id: 1,
        name: 'Uknown',
        phone: ''
      },
      created_at: new Date(),
      created_by: {
        id: 1,
        name: 'Unknown'
      },
      is_active: false
    })
  }
}
const actions = {
  addNewCart ({commit}, payload) {
    commit('incrementLastCartID')
    commit('addNewCart', payload)
  },
  setActiveCart ({commit}, payload) {
    commit('setActiveCart', payload)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
