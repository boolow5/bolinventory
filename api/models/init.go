package models

import (
	"fmt"
	"strings"

	"bitbucket.org/boolow5/bolinventory/api/config"
	"bitbucket.org/boolow5/bolinventory/api/db"
	"bitbucket.org/boolow5/bolinventory/api/logger"
	"gopkg.in/mgo.v2/bson"
)

// Init initializes models
func Init() error {
	createIndexes()
	var admin User
	err := db.FindOne(UserCollection, &bson.M{"level": AdminUserLevel}, &admin)
	if err != nil && !db.IsNotFound(err) {
		panic(fmt.Errorf("failed to get admin user. Reason: %v", err))
	}
	// count, err := db.Count(UserCollection, &bson.M{"level": AdminUserLevel})
	if db.IsNotFound(err) {
		ad := config.Get().Admin
		admin = User{
			Email:    ad.Email,
			Password: ad.Password,
			Level:    AdminUserLevel,
			Profile: Profile{
				FirstName: ad.FirstName,
				LastName:  ad.LastName,
			},
		}
		errs := Save(&admin)
		if len(errs) > 0 {
			errMessages := []string{}
			for _, err := range errs {
				errMessages = append(errMessages, string(err.Error()))
			}
			logger.Error(strings.Join(errMessages, ", "))
		}
	} else {
		fmt.Printf("Initializing users ERROR: %v\n", err)
	}
	return err
}

func createIndexes() {
	logger.Debug("*************** Adding indexes ***************")
	err := db.AddUniqueIndex(UserCollection, "email", "phone")
	if err != nil {
		logger.Errorf("Failed to add unique fields 'email,phone' to '%s' collection. Error: %v", UserCollection, err)
	}
}
