package main

import (
	"fmt"

	"bitbucket.org/boolow5/bolinventory/api/config"
	"bitbucket.org/boolow5/bolinventory/api/db"
	"bitbucket.org/boolow5/bolinventory/api/logger"
	"bitbucket.org/boolow5/bolinventory/api/models"
	"bitbucket.org/boolow5/bolinventory/api/router"
)

func main() {
	fmt.Printf("Bolinventory API %s\n\n", config.Get().Version)
	logger.Init(logger.DebugLevel)
	db.Init()
	models.Init()

	router := router.Init()
	router.Run(config.Get().Port)
}
