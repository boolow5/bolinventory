import Vue from 'vue'
import Vuex from 'vuex'

import app from './modules/app'
import user from './modules/user'
import transactions from './modules/transactions'

Vue.use(Vuex)

const modules = {
  app,
  user,
  transactions
}

export default new Vuex.Store({
  modules,
  strict: true
})
