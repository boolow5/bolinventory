const state = {
  token: '',
  userLevel: {
    level: 0,
    name: ''
  },
  profile: {}
}

const getters = {
  getUserLevel (state) {
    return state.userLevel
  },
  getIsDriver (state) {
    return state.userLevel.level === 6
  },
  getToken (state) {
    return state.token
  },
  getIsAuthenticated (state) {
    return (state.token && state.token.length > 50) || false
  },
  getProfile (state) {
    return state.profile
  }
}

const mutations = {
  setToken (state, payload) {
    if (payload && payload.length > 50) {
      state.token = payload
      window.localStorage.setItem('user_token', payload)
    }
  },
  setUserLevel (state, payload) {
    console.log('setUserLevel', payload)
    if (payload && typeof payload.level === 'number') {
      state.userLevel.level = payload.level
      state.userLevel.name = payload.level_name
      window.localStorage.setItem('user_level', payload)
    }
  },
  setProfile (state, payload) {
    if (typeof payload === 'object' && payload.hasOwnProperty('id')) {
      state.profile = payload
    }
  },
  clearUser (state, payload) {
    state.token = ''
    state.profile = {}
    state.userLevel = {
      level: 0,
      name: ''
    }
  }
}

const actions = {
  setUserLevel (context, payload) {
    context.commit('setUserLevel', payload)
  },
  setToken (context, payload) {
    context.commit('setToken', payload)
  },
  setProfile (context, payload) {
    context.commit('setProfile', payload)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
