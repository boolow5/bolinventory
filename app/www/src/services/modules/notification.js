import q from 'q'
import axios from 'axios'
import common from './common'

var NotifyNearbyDrivers = function (tripID, nearbyDistricts, price, distance, vehicleType) {
  var deferred = q.defer()
  if (!tripID) {
    deferred.reject(new Error('invalid trip id'))
  }
  if (!nearbyDistricts) {
    deferred.reject(new Error('invalid nearby districts'))
  }
  if (!price) {
    deferred.reject(new Error('invalid price'))
  }
  if (!distance) {
    deferred.reject(new Error('invalid distance'))
  }
  if (!vehicleType) {
    deferred.reject(new Error('invalid vehicle type'))
  }
  axios({
    method: 'POST',
    url: common.getFullURL('/notify-nearby-driver'),
    data: {
      trip_id: tripID,
      nearbyDistricts,
      price,
      distance,
      vehicle_type: vehicleType
    },
    headers: { 'Authorization': '' + common.getToken() || localStorage.getItem('user_token') }
  }).then(resp => {
    deferred.resolve(resp)
  }).catch(err => {
    deferred.reject(err)
  })
  return deferred.promise
}

export default {
  NotifyNearbyDrivers
}
