package models

import (
	"fmt"
	"time"

	"gopkg.in/mgo.v2/bson"
)

// Cart represents an inventory product or a shop
type Cart struct {
	ID     bson.ObjectId `json:"id" bson:"_id"`
	UserID bson.ObjectId `json:"user_id" bson:"user_id"`

	Items []Item `json:"items" bson:"items"`

	Timestamp `bson:",inline"`
}

// Item represents an product item in a cart
type Item struct {
	Product  Product
	Quantity int
}

// GetColName satisfies DBObject interface
func (c Cart) GetColName() string {
	return CartCollection
}

// GetID satisfies DBObject interface
func (c Cart) GetID() bson.ObjectId {
	return c.ID
}

// SetID satisfies DBObject interface
func (c *Cart) SetID(id bson.ObjectId) {
	c.ID = id
}

// SetTimestamp satisfies DBObject interface
func (c *Cart) SetTimestamp(isCreate bool) {
	now := time.Now()
	if isCreate {
		c.CreatedAt = now
		c.UpdatedAt = now
	} else {
		c.UpdatedAt = now
	}
}

// IsValid satisfies DBObject interface
func (c Cart) IsValid() (valid bool, errs []error) {
	valid = bson.IsObjectIdHex(c.ID.Hex())
	if !valid {
		return valid, []error{fmt.Errorf("Invalid product id")}
	}
	return valid, nil
}

// AutoSetID satisfies DBObject interface, to control setting item ID behavior
func (c Cart) AutoSetID() bool {
	return true
}
