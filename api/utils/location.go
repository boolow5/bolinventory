package utils

import (
	"fmt"
	"math"

	"bitbucket.org/boolow5/bolinventory/api/config"
	"bitbucket.org/boolow5/bolinventory/api/logger"
	"github.com/codingsince1985/geo-golang"
	"github.com/codingsince1985/geo-golang/google"
	"github.com/codingsince1985/geo-golang/openstreetmap"
)

const (
	// BackendOSMaps means request was returned by OpenStreetMaps
	BackendOSMaps = "osmaps"
	// BackendGMaps means request was returned by Google Maps
	BackendGMaps = "gmaps"
	// BackendBInventory means the request was returned by our server
	BackendBInventory = "bolinventory"
)

// GeoCode takes an address and returns coordinates
func GeoCode(address string) (*geo.Location, string, error) {
	location, err := openstreetmap.Geocoder().Geocode(address)
	if err == nil && location != nil && location.Lat != 0 && location.Lng != 0 {
		return location, BackendOSMaps, nil
	}
	logger.Errorf("OSM <location: %v >\n error: %v", location, err)
	location, err = google.Geocoder(config.Get().MapKey).Geocode(address)
	if err == nil && location != nil && location.Lat != 0 && location.Lng != 0 {
		return location, BackendGMaps, nil
	}
	logger.Errorf("GMAP <location: %v >\n error: %v", location, err)
	if location == nil {
		return location, BackendBInventory, fmt.Errorf("invalid location response")
	}
	return location, BackendBInventory, err
}

// ReverseGeocode takes coordinates and returns address
func ReverseGeocode(lat, long float64) (*geo.Address, string, error) {
	address, err := openstreetmap.Geocoder().ReverseGeocode(lat, long)
	if err == nil && address != nil {
		return address, BackendOSMaps, nil
	}
	logger.Errorf("OSM <address: %v >\n error: %v", address, err)
	address, err = google.Geocoder(config.Get().MapKey).ReverseGeocode(lat, long)
	if err == nil && address != nil {
		return address, BackendGMaps, nil
	}
	logger.Errorf("GMAP <address: %v >\n error: %v", address, err)
	if address == nil {
		return address, BackendBInventory, fmt.Errorf("invalid address returned")
	}
	return address, BackendBInventory, err
}

// CalculateDistance calcuates distance between two points on earth
func CalculateDistance(lat1, long1, lat2, long2 float64) float64 {
	R := 6371.0
	dLat := toRadian(lat2 - lat1)
	dLon := toRadian(long2 - long1)

	a := math.Sin(dLat/2)*math.Sin(dLat/2) +
		math.Cos(toRadian(lat1))*math.Cos(toRadian(lat2))*
			math.Sin(dLon/2)*math.Sin(dLon)
	c := 2 * math.Atan2(math.Sqrt(a), math.Sqrt(1-a))
	distance := R * c
	return distance
}

func toRadian(num float64) float64 {
	return num * math.Pi / 180
}

// DistanceInKm calculates distance in KM
func DistanceInKm(lat1, long1, lat2, long2 float64) float64 {
	earthRadiusKm := 6371.0

	dLat := toRadian(lat2 - lat1)
	dLon := toRadian(long2 - long1)

	lat1 = toRadian(lat1)
	lat2 = toRadian(lat2)

	a := math.Sin(dLat/2)*math.Sin(dLat/2) +
		math.Sin(dLon/2)*math.Sin(dLon/2)*math.Cos(lat1)*math.Cos(lat2)
	c := 2 * math.Atan2(math.Sqrt(a), math.Sqrt(1-a))
	return earthRadiusKm * c
}
