package models

import (
	"fmt"
	"time"

	"bitbucket.org/boolow5/bolinventory/api/db"
	"bitbucket.org/boolow5/bolinventory/api/logger"
	"gopkg.in/mgo.v2/bson"
)

var (
	// Version of the last releases
	Version string
	// CommitNo last git commit number
	CommitNo string
	// LastBuidDate the last deployment date
	LastBuidDate string
)

// DBObject defines the behavior of object that can be saved to database
type DBObject interface {
	GetColName() string
	GetID() bson.ObjectId
	SetID(bson.ObjectId)
	SetTimestamp(bool)
	IsValid() (bool, []error)
	AutoSetID() bool
}

// Timestamp is an object to be embedded in other objects for tracking date time
type Timestamp struct {
	CreatedAt time.Time `json:"created_at" bson:"created_at"`
	UpdatedAt time.Time `json:"updated_at" bson:"updated_at"`
}

// Save stores an item in the database
func Save(item DBObject) []error {
	if item.AutoSetID() {
		item.SetID(bson.NewObjectId())
	}
	logger.Debugf("Save(item: %s) ID: [%s]", item.GetColName(), item.GetID().Hex())
	if valid, errs := item.IsValid(); errs != nil || !valid {
		logger.Error("Errors:", errs)
		return append([]error{fmt.Errorf("%s is not valid", item.GetColName())}, errs...)
	}
	item.SetTimestamp(true)
	err := db.CreateStrong(item.GetColName(), item)
	if err != nil {
		return []error{err}
	}
	return nil
}

// Update stores changes to an item in the database
func Update(item DBObject) []error {
	item.SetTimestamp(false)
	if valid, errs := item.IsValid(); errs != nil || !valid {
		return append([]error{fmt.Errorf("%s is not valid", item.GetColName())}, errs...)
	}
	err := db.UpdateStrong(item.GetColName(), item.GetID(), item)
	if err != nil {
		return []error{err}
	}
	return nil
}

// Remove deletes an item from the database
func Remove(item DBObject) error {
	return db.DeleteId(item.GetColName(), item.GetID())
}

// GetItemByID finds DBObject item by it's ID
func GetItemByID(id bson.ObjectId, output DBObject) error {
	return db.FindOne(output.GetColName(), &bson.M{"_id": id}, output)
}

// GetItemsByFilter finds DBObject items by it's ID
func GetItemsByFilter(colName string, filter *bson.M, output []DBObject) error {
	return db.FindAll(colName, filter, output)
}

// GetItemByFilter finds DBObject item by it's ID
func GetItemByFilter(colName string, filter *bson.M, output DBObject) error {
	return db.FindOne(colName, filter, output)
}
