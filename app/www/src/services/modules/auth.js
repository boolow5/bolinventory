import q from 'q'
import axios from 'axios'
import common from './common'

var Login = function (user) {
  console.log('Login', user)
  var deferred = q.defer()
  if (!user) {
    deferred.reject('Invalid user')
  } else if (user && !user.hasOwnProperty('email')) {
    deferred.reject('invalid email')
  } else if (user && !user.hasOwnProperty('password')) {
    deferred.reject('invalid password')
  }
  axios({
    method: 'POST',
    url: common.getFullURL('/login'),
    data: user
  }).then(resp => {
    deferred.resolve(resp)
  }).catch(err => {
    deferred.reject(err)
  })
  return deferred.promise
}

var Signup = function (user) {
  console.log('Signup', user)
  var deferred = q.defer()
  if (!user) {
    deferred.reject('Invalid user')
  }
  axios({
    method: 'POST',
    url: common.getFullURL('/signup'),
    data: user
  }).then(resp => {
    deferred.resolve(resp)
  }).catch(err => {
    deferred.reject(err)
  })
  return deferred.promise
}

var GetProfile = function () {
  var deferred = q.defer()
  axios({
    method: 'GET',
    url: common.getFullURL('/profile'),
    headers: { 'Authorization': '' + common.getToken() || localStorage.getItem('user_token') }
  }).then(resp => {
    deferred.resolve(resp)
  }).catch(err => {
    deferred.reject(err)
  })
  return deferred.promise
}

var GetUserLevel = function () {
  var deferred = q.defer()
  axios({
    method: 'GET',
    url: common.getFullURL('/user-level'),
    headers: { 'Authorization': '' + common.getToken() || localStorage.getItem('user_token') }
  }).then(resp => {
    deferred.resolve(resp)
  }).catch(err => {
    deferred.reject(err)
  })
  return deferred.promise
}

var Upload = function (file, options) {
  var deferred = q.defer()
  let url = common.getFullURL('/upload')
  if (options) {
    if (options.vehicleID && options.vehicleID.length !== 24) {
      deferred.reject('invalid vehicle id')
    } else if (options.vehicleID && options.vehicleID.length === 24) {
      url = common.AppendQuery(url, 'vehicleID', options.vehicleID)
    } else if (options.driverID && options.driverID.length !== 24) {
      deferred.reject('invalid vehicle id')
    } else if (options.driverID && options.driverID.length === 24) {
      url = common.AppendQuery(url, 'driverID', options.driverID)
    }
  }
  axios({
    method: 'POST',
    url: url,
    headers: {
      'Authorization': '' + common.getToken() || localStorage.getItem('user_token'),
      'Content-Type': 'multipart/form-data'
    },
    data: file
  }).then(resp => {
    deferred.resolve(resp)
  }).catch(err => {
    deferred.reject(err)
  })
  return deferred.promise
}

var GetVersion = function () {
  var deferred = q.defer()
  axios({
    method: 'GET',
    url: common.getFullURL('/version'),
    headers: { 'Authorization': '' + common.getToken() || localStorage.getItem('user_token') }
  }).then(resp => {
    deferred.resolve(resp)
  }).catch(err => {
    deferred.reject(err)
  })
  return deferred.promise
}

var GetSettings = function () {
  var deferred = q.defer()
  axios({
    method: 'GET',
    url: common.getFullURL('/settings'),
    headers: { 'Authorization': '' + common.getToken() || localStorage.getItem('user_token') }
  }).then(resp => {
    deferred.resolve(resp)
  }).catch(err => {
    deferred.reject(err)
  })
  return deferred.promise
}

var AddSettings = function (settings) {
  var deferred = q.defer()
  if (!settings) {
    deferred.reject(new Error('invalid settings form data'))
  }
  axios({
    method: 'POST',
    url: common.getFullURL('/settings'),
    data: settings,
    headers: { 'Authorization': '' + common.getToken() || localStorage.getItem('user_token') }
  }).then(resp => {
    deferred.resolve(resp)
  }).catch(err => {
    deferred.reject(err)
  })
  return deferred.promise
}

export default {
  Login,
  Signup,
  GetProfile,
  GetUserLevel,
  Upload,
  GetVersion,
  GetSettings,
  AddSettings
}
