package models

import (
	"fmt"
	"time"

	"gopkg.in/mgo.v2/bson"
)

// Product represents an inventory product or a shop
type Product struct {
	ID        bson.ObjectId `json:"id" bson:"_id"`
	Name      string        `json:"name" bson:"name"`
	Cost      float32       `json:"cost" bson:"cost"`
	Price     float32       `json:"price" bson:"price"`
	BarcodeID string        `json:"barcode_id" bson:"barcode_id"`
	Images    []Image       `json:"images" bson:"images"`

	StoreInventories []StoreInventory `json:"store_inventories" bson:"store_inventories"`

	Timestamp `bson:",inline"`
}

// StoreInventory is the number of product items in each store
type StoreInventory struct {
	StoreID  bson.ObjectId
	Quantity int
}

// GetColName satisfies DBObject interface
func (p Product) GetColName() string {
	return ProductCollection
}

// GetID satisfies DBObject interface
func (p Product) GetID() bson.ObjectId {
	return p.ID
}

// SetID satisfies DBObject interface
func (p *Product) SetID(id bson.ObjectId) {
	p.ID = id
}

// SetTimestamp satisfies DBObject interface
func (p *Product) SetTimestamp(isCreate bool) {
	now := time.Now()
	if isCreate {
		p.CreatedAt = now
		p.UpdatedAt = now
	} else {
		p.UpdatedAt = now
	}
}

// IsValid satisfies DBObject interface
func (p Product) IsValid() (valid bool, errs []error) {
	valid = bson.IsObjectIdHex(p.ID.Hex())
	if !valid {
		return valid, []error{fmt.Errorf("Invalid product id")}
	}
	return valid, nil
}

// AutoSetID satisfies DBObject interface, to control setting item ID behavior
func (p Product) AutoSetID() bool {
	return true
}
