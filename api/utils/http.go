package utils

import (
	"fmt"
	"strings"

	"github.com/gin-gonic/gin"
)

// AbortWithError is shortcut for aborting http request with error message and error code
func AbortWithError(c *gin.Context, status int, err error, warnings ...string) {
	msg := struct {
		Msg      string   `json:"msg"`
		Warnings []string `json:"warnings"`
	}{
		Msg:      err.Error(),
		Warnings: warnings,
	}
	c.JSON(status, msg)
	c.AbortWithError(status, err)
}

// AbortWithErrors is shortcut for aborting http request with a list of error messages
func AbortWithErrors(c *gin.Context, status int, errs []error, warnings ...string) {
	response := struct {
		Messages []string `json:"messages"`
		Warnings []string `json:"warnings"`
	}{
		Warnings: warnings,
	}
	for _, err := range errs {
		response.Messages = append(response.Messages, []string{err.Error()}...)
	}
	messages := strings.Join(response.Messages, ", ")
	c.JSON(status, response)
	c.AbortWithError(status, fmt.Errorf(messages))
}
