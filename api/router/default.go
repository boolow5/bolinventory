package router

import (
	"bitbucket.org/boolow5/bolinventory/api/middlewares"
	"github.com/gin-gonic/gin"
)

// Init initializes API routes, endpionts and middlware integrations
func Init() *gin.Engine {
	router := gin.Default()
	// set maximum file size
	router.MaxMultipartMemory = 1 << 20
	router.Use(middlewares.CORS())
	setupV1(router)
	return router
}
