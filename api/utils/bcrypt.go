package utils

import (
	"golang.org/x/crypto/bcrypt"
)

// BcryptHash taks string as password and return hashed password and error if there is any
func BcryptHash(password string) (string, error) {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	return string(hashedPassword), err
}

// AuthenticatePassword compares bcrypt hash and password and returns true if they match
func AuthenticatePassword(hash, password string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	if err != nil {
		return false
	}
	return true
}
