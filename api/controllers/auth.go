package controllers

import (
	"errors"
	"net/http"

	"bitbucket.org/boolow5/bolinventory/api/logger"
	"bitbucket.org/boolow5/bolinventory/api/models"
	"bitbucket.org/boolow5/bolinventory/api/utils"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

// Register adds new user if current user is admin
func Register(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	admin := models.User{}
	err := models.GetItemByID(userID, &admin)
	if err != nil {
		logger.Error("finding admin by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}
	if admin.Level < models.FifthUserLevel {
		logger.Errorf("user level very low: %d", admin.Level)
		AbortWithError(c, http.StatusBadRequest, NewError(PermissionError, err.Error()))
		return
	}
	input := struct {
		Email      string `json:"email"`
		Phone      string `json:"phone"`
		Password   string `json:"password"`
		FirstName  string `json:"first_name"`
		MiddleName string `json:"middle_name"`
		LastName   string `json:"last_name"`
		Gender     string `json:"gender"`
		ProfilePic string `json:"profile_pic"`
	}{}
	err = c.Bind(&input)
	if err != nil {
		logger.Error("binding error: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, err.Error()))
		return
	}
	user := models.User{
		Email:    input.Email,
		Phone:    input.Phone,
		Password: input.Password,
		Profile: models.Profile{
			FirstName:  input.FirstName,
			MiddleName: input.MiddleName,
			LastName:   input.LastName,
			Gender:     input.Gender,
			ProfilePic: input.ProfilePic,
		},
	}
	errs := models.Save(&user)
	if errs != nil {
		logger.Error("saving error: ", err)
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(SavingItemError, errs))
		return
	}
	c.JSON(http.StatusOK, gin.H{"user": user})
}

// Login registers new user
func Login(c *gin.Context) {
	logger.Debugf("Login")
	// userID := bson.ObjectIdHex(c.MustGet("id").(string))
	input := struct {
		Email    string `json:"email"`
		Phone    string `json:"phone"`
		Password string `json:"password"`
	}{}
	err := c.Bind(&input)
	if err != nil {
		logger.Errorf("Error binding input: %v ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(MissingFieldError, err.Error()))
		return
	}
	missingFieldErrors := []error{}

	if len(input.Password) < 6 {
		logger.Error("password is too short: ", len(input.Password))
		missingFieldErrors = append(missingFieldErrors, []error{errors.New("password is too short")}...)
	}
	// check if already is registered
	// user := models.User{
	// 	Email:    input.Email,
	// 	Password: input.Password,
	// }
	// user, err := models.GetUserByEmail(input.Email)
	user := models.User{}
	isClean, phoneNumber := utils.CleanPhoneNumber(input.Phone)
	if isClean {
		input.Phone = phoneNumber
	}
	emailIsValid := utils.IsValidEmail(input.Email)
	phoneIsValid := utils.IsValidPhone(input.Phone)
	if emailIsValid {
		err = models.GetItemByFilter(user.GetColName(), &bson.M{"email": input.Email}, &user)
	} else if phoneIsValid {
		err = models.GetItemByFilter(user.GetColName(), &bson.M{"phone": input.Phone}, &user)
	} else {
		logger.Errorf("invalid phone %v or email %v: %v", !emailIsValid, !phoneIsValid, err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, "invalid phone or email"))
		return
	}
	if err != nil {
		logger.Error("login failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}
	logger.Debugf("USER: %+v \nFound by email? %v or phone? %v", user, emailIsValid, phoneIsValid)

	ok, err := user.Authenticate(input.Password)
	logger.Debugf("User Authenticate: is OK? %v, ERROR: %+v ", ok, err)
	if err != nil {
		logger.Error("login failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}
	if !ok {
		logger.Error("login failed: not OK")
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, "Not OK"))
		return
	}
	token, err := user.GetToken()
	if err != nil {
		logger.Error("token generation failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(TokenGenerationError, err.Error()))
		return
	}
	c.JSON(http.StatusOK, gin.H{"token": token, "level": user.Level})
}

// GetProfile finds the current user's profile
func GetProfile(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	user := models.User{}
	err := models.GetItemByID(userID, &user)
	if err != nil {
		logger.Error("finding user by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}
	c.JSON(http.StatusOK, gin.H{"profile": user.Profile})
}

// GetUserLevel checks if current user's level
func GetUserLevel(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	user := models.User{}
	err := models.GetItemByID(userID, &user)
	if err != nil {
		logger.Error("finding user by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}
	switch user.Level {
	case models.LowestUserLevel:
		c.JSON(http.StatusOK, gin.H{"level": user.Level, "level_name": "lowest"})
	case models.NormalUserLevel:
		c.JSON(http.StatusOK, gin.H{"level": user.Level, "level_name": "normal"})
	case models.SecondUserLevel:
		c.JSON(http.StatusOK, gin.H{"level": user.Level, "level_name": "second"})
	case models.ThirdUserLevel:
		c.JSON(http.StatusOK, gin.H{"level": user.Level, "level_name": "third"})
	case models.FourthUserLevel:
		c.JSON(http.StatusOK, gin.H{"level": user.Level, "level_name": "fourth"})
	case models.FifthUserLevel:
		c.JSON(http.StatusOK, gin.H{"level": user.Level, "level_name": "fifth"})
	case models.AdminUserLevel:
		c.JSON(http.StatusOK, gin.H{"level": user.Level, "level_name": "admin"})
	}
}
