import 'babel-polyfill'
import Vue from 'vue'
import App from './App.vue'
import Cordova from './Cordova.js'

import store from './store'
import router from './router'
import { sync } from 'vuex-router-sync'
import i18n from 'vuex-i18n'

import Arabic from '../translations/ar'
import Somali from '../translations/so'

sync(store, router)
Vue.use(i18n.plugin, store)
Vue.i18n.add('ar', Arabic)
Vue.i18n.add('so', Somali)

let lang = localStorage.getItem('lang')
Vue.i18n.set(lang || 'en')

// Uniform event channel
export const EventBus = new Vue()

// Load Vue instance
export default new Vue({
  router,
  store,
  el: '#app',
  render: h => h(App),
  mounted () {
    Cordova.initialize()
  }
})
