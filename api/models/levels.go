package models

const (
	// LowestUserLevel means user not verified yet
	LowestUserLevel = iota
	// NormalUserLevel means user has level 1 clearance
	NormalUserLevel
	// SecondUserLevel means user has level 2 clearance
	SecondUserLevel
	// ThirdUserLevel means user has level 3 clearance
	ThirdUserLevel
	// FourthUserLevel means user has level 4 clearance
	FourthUserLevel
	// FifthUserLevel means user has level 5 clearance
	FifthUserLevel
	// AdminUserLevel means user has level 6 clearance
	AdminUserLevel
)
