import Vue from 'vue'
import Router from 'vue-router'

// const Foo = resolve => require(['../views/hello.vue'], resolve);
// import hello from '../views/hello.vue';

import main from '../views/main.vue'
import about from '../views/about.vue'
import signup from '../views/signup.vue'
import login from '../views/login.vue'
import cart from '../views/cart.vue'
import product from '../views/product.vue'
import products from '../views/products.vue'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    { name: 'home', path: '/', component: main },
    { name: 'about', path: '/about', component: about },
    { name: 'signup', path: '/signup', component: signup },
    { name: 'login', path: '/login', component: login },
    { name: 'cart', path: '/cart', component: cart },
    { name: 'product', path: '/product/:id', component: product },
    { name: 'products', path: '/products', component: products }
  ]
})
